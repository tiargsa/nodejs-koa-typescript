import * as passport from "koa-passport";
import * as LocalPassport from "passport-local";

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(async function(id, done) {
  done(null, { id });
});

passport.use(
  new LocalPassport.Strategy(function(username: string, password: string, done: any): void {
    if (username === "sebastianlube@gmail.com" && password === "12341234") {
      done(null, { id: 1, username });
    } else {
      done(null, false);
    }
  })
);

export default passport;
