import * as cors from 'kcors';
import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as respond from 'koa-respond';
import * as session from 'koa-session';
import logger from 'lib/logger';
import passport from 'lib/passport';
import errorHandler from 'middleware/error';
import serve from 'middleware/serve';
import router from 'modules';

export default (async function createServer(): Promise<Koa> {
  logger.debug("Creating server...", { scope: "startup" });
  const app = new Koa();

  app.keys = ["secret"];
  app.use(session({}, app));

  app.use(respond());
  app.use(cors());
  app.use(bodyParser());

  app.use(passport.initialize());
  app.use(passport.session());
  app.use(errorHandler);

  app.use(serve);

  app.use(router.allowedMethods());
  app.use(router.routes());

  logger.debug("Server created, ready to listen", { scope: "startup" });
  return app;
});
