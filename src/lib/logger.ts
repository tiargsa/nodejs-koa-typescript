import * as winston from 'winston';

var logger = new (winston.Logger)({
  transports: [
    new winston.transports.Console({
      colorize: true
    }),
    new winston.transports.File({
      name: 'app',
      filename: 'logs/app.log'
    }),
    new winston.transports.File({
      name: 'error',
      level: 'error',
      filename: 'logs/exceptions.log',
      handleExceptions: true,
      humanReadableUnhandledException: true 
    })
  ]
});

export default logger;
