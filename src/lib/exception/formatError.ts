import env from 'lib/env';

interface StreamableError {
  status: number;
  message: string;
  stack?: string;
}

export default function formatError(originalError: StreamableError): StreamableError {
  try {
    if (env.NODE_ENV !== "production") {
      return {
        status: originalError.status || 500,
        message: originalError.message,
        stack: originalError.stack
      };
    } else {
      return {
        status: originalError.status || 500,
        message: originalError.message ? originalError.message : "Internal Server Error"
      };
    }
  } catch (e) {
    return {
      status: 500,
      message: env.NODE_ENV !== "production" ? e.message : "Internal Server Error"
    };
  }
}
