import env from 'lib/env';

class RequestFormatError extends Error {
  status: number;

  constructor(message: string, status: number) {
    super(message);

    Object.setPrototypeOf(this, RequestFormatError.prototype);

    this.name = "Format Request Error";
    this.status = status;
  }
}

export default function formatRequestError(originalMessage: string) {
  const prodMessage = "Error de validación de request";
  const message = env.NODE_ENV !== "production" ? `${prodMessage} ${JSON.stringify(originalMessage)}` : prodMessage;

  return new RequestFormatError(message, 400);
}
