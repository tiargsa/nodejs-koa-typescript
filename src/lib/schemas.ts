import * as Ajv from 'ajv';
import * as Koa from 'koa';
import formatRequestError from 'lib/exception/requestError';

const ajv = new Ajv();

export const compile = ajv.compile.bind(ajv);

export interface SchemaType {
  [key: string]: Ajv.ValidateFunction;
}

export default (async function validarSchema(schemas: SchemaType, actionName: string, request: Koa.Request): Promise<boolean> {
  try {
    return schemas[actionName](request.body);
  } catch (e) {
    throw formatRequestError(e.errors);
  }
});
