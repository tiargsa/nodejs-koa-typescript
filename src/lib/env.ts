import * as yenv from 'yenv';

interface Enviroment {
  NODE_ENV: string;
}

const env = {
  NODE_ENV: process.env["NODE_ENV"] || "development",
  load: (newEnv: any): Enviroment => Object.assign(env, yenv("env.yaml", { env: newEnv })),
  ...yenv()
};

export default env;
