import 'reflect-metadata';

import * as Koa from 'koa';
import createServer from 'lib/createServer';
import env from 'lib/env';
import logger from 'lib/logger';
import * as TypeORM from 'typeorm';

Object.keys(env).map(key => (process.env[key] = env[key]));

const PORT = process.env.PORT || 1338;

async function start(): Promise<void> {
  try {
    const app: Koa = await createServer();
    app.context.connection = await TypeORM.createConnection();

    app.listen(PORT, () => {
      logger.debug(`Server listening on ${PORT} in ${process.env.NODE_ENV} mode`);
    });
  } catch (e) {
    logger.error("Error while starting up server", e);
    process.exit(1);
  }
}

start();
