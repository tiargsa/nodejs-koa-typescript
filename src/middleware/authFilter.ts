import * as Koa from 'koa';

function authFilter(ctx: Koa.Context, next: () => Promise<any>): Promise<Koa.Middleware> | undefined {
  if (ctx.isAuthenticated()) {
    return next();
  } else {
    ctx.status = 403;
  }
}

export default authFilter;
