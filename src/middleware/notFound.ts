import * as Koa from 'koa';

function notFoundHandler(ctx: Koa.BaseContext): void {
  ctx.status = 404;
  ctx.body = "Whatever you were looking for, we ain't got it, son.";
}

export default notFoundHandler;
