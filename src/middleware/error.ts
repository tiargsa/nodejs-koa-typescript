import * as Koa from 'koa';
import formatError from 'lib/exception/formatError';
import logger from 'lib/logger';

async function error(ctx: Koa.Context, next: () => Promise<any>): Promise<Koa.Middleware | void> {
  try {
    await next();
  } catch (e) {
    logger.error(e);

    const { status, message, stack } = formatError(e);
    const error = {
      error: {
        message,
        stack: stack
      }
    };

    ctx.status = status;
    ctx.body = error;
  }
}

process.on("uncaughtException", function(e) {
  logger.error("uncaught", e);
});

export default error;
