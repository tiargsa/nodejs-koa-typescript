import { stat } from 'fs';
import * as Koa from 'koa';
import * as send from 'koa-send';
import { join } from 'path';
import { promisify } from 'util';

let pStat = promisify(stat);

async function sendMiddleware(ctx: Koa.Context, next: () => Promise<any>): Promise<void> {
  if (ctx.path === "/") {
    await send(ctx, "index.html", { root: join(__dirname, "../app/client") });
  } else if (ctx.path === "/admin") {
    await send(ctx, "index.html", { root: join(__dirname, "../app/dashboard") });
  } else {
    if (!ctx.path.match(/\/api\/.+/g)) {
      try {
        await pStat(join(__dirname, "../app/client", ctx.path));
        await send(ctx, ctx.path, { root: join(__dirname, "../app/client") });
      } catch (e) {
        await pStat(join(__dirname, "../app/dashboard", ctx.path));
        await send(ctx, ctx.path, { root: join(__dirname, "../app/dashboard") });
      }
    }
  }

  await next();
}

export default sendMiddleware;
