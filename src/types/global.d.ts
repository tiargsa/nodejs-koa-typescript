import * as TypeORM from 'typeorm';

declare module "koa" {
  interface BaseContext {
    connection: TypeORM.Connection;
  }
}
