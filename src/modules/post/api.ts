import * as Koa from 'koa';
import * as Router from 'koa-router';
import validarSchema from 'lib/schemas';

import AuthenticationSchemas from './schemas';
import { listarPosts } from './service';

const router = new Router();

router.get("/", async function listPostsAction(ctx: Koa.Context, next: () => Promise<any>) {
  validarSchema(AuthenticationSchemas, "listar", ctx.request.query);

  ctx.body = listarPosts(ctx, ctx.request.query);
});

export default router;
