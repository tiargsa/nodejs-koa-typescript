import * as Koa from 'koa';
import { getRepository } from 'typeorm';

import Post from './entity';

interface PageableSchema {
  offset: number;
  limit: number;
}

export async function listarPosts(context: Koa.BaseContext, { offset, limit }: PageableSchema) {
  try {
    return getRepository(Post).find();
  } catch (e) {
    throw e;
  }
}
