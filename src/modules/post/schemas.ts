import { compile, SchemaType } from 'lib/schemas';

const PostSchema = {
  properties: {
    titulo: { type: "string" },
    contenido: { type: "string" }
  },
  required: ["titulo", "contenido"]
};

const FiltroPostSchema = {
  properties: {
    offset: { type: "number" },
    limit: { type: "number" }
  },
  required: ["offset", "limit"],
  additionalProperties: false
};

const PostSchemas: SchemaType = {
  crear: compile(PostSchema),
  editar: compile(PostSchema),
  listar: compile(FiltroPostSchema)
};

export default PostSchemas;
