import { compile, SchemaType } from 'lib/schemas';

const LoginSchema = {
  properties: {
    username: { type: "string", format: "email" },
    password: { type: "string" }
  },
  required: ["username", "password"],
  additionalProperties: false
};

const AuthenticationSchemas: SchemaType = {
  login: compile(LoginSchema)
};

export default AuthenticationSchemas;
