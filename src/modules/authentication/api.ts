import * as Koa from 'koa';
import * as Router from 'koa-router';
import passport from 'lib/passport';
import validarSchema from 'lib/schemas';

import AuthenticationSchemas from './schemas';

const router = new Router();

router.post("/login", async function loginAction(ctx: Koa.Context, next: () => Promise<any>) {
  validarSchema(AuthenticationSchemas, "login", ctx.request.body);

  ctx.query = ctx.request.body;

  return passport.authenticate("local", async function(err: Error, user: object | boolean, info: object, status: number): Promise<void> {
    if (user === false) {
      ctx.body = { success: false };
      ctx.throw(401);
    } else {
      ctx.body = { success: true };
      await ctx.login(user);
    }
  })(ctx, next);
});

export default router;
