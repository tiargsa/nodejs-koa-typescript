import * as Router from 'koa-router';
import authFilter from 'middleware/authFilter';
import authentication from 'modules/authentication/api';
import post from 'modules/post/api';

const router = new Router();
const API_ROOT_PATH = "/api";

router.use(`${API_ROOT_PATH}`, authentication.routes(), authentication.allowedMethods());
router.use(`${API_ROOT_PATH}/post`, post.routes(), post.allowedMethods());

router.use(authFilter);

export default router;
